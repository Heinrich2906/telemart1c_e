﻿#Если Клиент Тогда
	
////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ НАЧАЛЬНОЙ НАСТРОЙКИ ОТЧЕТА

// Процедура установки начальных настроек отчета по метаданным регистра накопления
//
Процедура УстановитьНачальныеНастройки(ДополнительныеПараметры = Неопределено) Экспорт
	
	// Настройка общих параметров универсального отчёта
	УправлениеОтчетами.ВосстановитьРеквизитыОтчета(ЭтотОбъект, ДополнительныеПараметры);
	
	// Содержит название отчёта, которое будет выводиться в шапке.
	// Тип: Строка.
	// Пример:
	// УниверсальныйОтчет.мНазваниеОтчета = "Название отчёта";
	УниверсальныйОтчет.мНазваниеОтчета = СокрЛП(ЭтотОбъект.Метаданные().Синоним);
	
	// Содержит признак необходимости отображения надписи и поля выбора раздела учёта в форме настройки.
	// Тип: Булево.
	// Значение по умолчанию: Истина.
	// Пример:
	// УниверсальныйОтчет.мВыбиратьИмяРегистра = Ложь;
	УниверсальныйОтчет.мВыбиратьИмяРегистра = Ложь;
	
	// Содержит имя регистра, по метаданным которого будет выполняться заполнение настроек отчёта.
	// Тип: Строка.
	// Пример:
	// УниверсальныйОтчет.ИмяРегистра = "ТоварыНаСкладах";
	УниверсальныйОтчет.ИмяРегистра = "";
	
	// Содержит признак необходимости вывода отрицательных значений показателей красным цветом.
	// Тип: Булево.
	// Значение по умолчанию: Ложь.
	// Пример:
	// УниверсальныйОтчет.ОтрицательноеКрасным = Истина;
	УниверсальныйОтчет.ОтрицательноеКрасным = Истина;
	
	// Содержит признак необходимости вывода в отчёт общих итогов.
	// Тип: Булево.
	// Значение по умолчанию: Ложь.
	// Пример:
	// УниверсальныйОтчет.ВыводитьОбщиеИтоги = Ложь;
	
	// Содержит признак необходимости вывода детальных записей в отчёт.
	// Тип: Булево.
	// Значение по умолчанию: Ложь.
	// Пример:
	// УниверсальныйОтчет.ВыводитьДетальныеЗаписи = Истина;
	
	// Содержит признак необходимости отображения флага использования свойств и категорий в форме настройки.
	// Тип: Булево.
	// Значение по умолчанию: Истина.
	// Пример:
	// УниверсальныйОтчет.мВыбиратьИспользованиеСвойств = Ложь;
	УниверсальныйОтчет.мВыбиратьИспользованиеСвойств = Истина;
	
	// Содержит признак использования свойств и категорий при заполнении настроек отчёта.
	// Тип: Булево.
	// Значение по умолчанию: Истина.
	// Пример:
	// УниверсальныйОтчет.ИспользоватьСвойстваИКатегории = Истина;
	УниверсальныйОтчет.ИспользоватьСвойстваИКатегории = Истина;
	
	// Содержит признак использования простой формы настроек отчёта без группировок колонок.
	// Тип: Булево.
	// Значение по умолчанию: Ложь.
	// Пример:
	// УниверсальныйОтчет.мРежимФормыНастройкиБезГруппировокКолонок = Истина;
	
	// Дополнительные параметры, переданные из отчёта, вызвавшего расшифровку.
	// Информация, передаваемая в переменной ДополнительныеПараметры, может быть использована
	// для реализации специфичных для данного отчета параметрических настроек.
	
	// Описание исходного текста запроса.
	// При написании текста запроса рекомендуется следовать правилам, описанным в следующем шаблоне текста запроса:
	//
	//ВЫБРАТЬ
	//	<ПсевдонимТаблицы.Поле> КАК <ПсевдонимПоля>,
	//	ПРЕДСТАВЛЕНИЕ(<ПсевдонимТаблицы>.<Поле>),
	//	<ПсевдонимТаблицы.Показатель> КАК <ПсевдонимПоказателя>
	//	//ПОЛЯ_СВОЙСТВА
	//	//ПОЛЯ_КАТЕГОРИИ
	//{ВЫБРАТЬ
	//	<ПсевдонимПоля>.*,
	//	<ПсевдонимПоказателя>,
	//	Регистратор,
	//	Период,
	//	ПериодДень,
	//	ПериодНеделя,
	//	ПериодДекада,
	//	ПериодМесяц,
	//	ПериодКвартал,
	//	ПериодПолугодие,
	//	ПериодГод
	//	//ПОЛЯ_СВОЙСТВА
	//	//ПОЛЯ_КАТЕГОРИИ
	//}
	//ИЗ
	//	<Таблица> КАК <ПсевдонимТаблицы>
	//	//СОЕДИНЕНИЯ
	//{ГДЕ
	//	<ПсевдонимТаблицы.Поле>.* КАК <ПсевдонимПоля>,
	//	<ПсевдонимТаблицы.Показатель> КАК <ПсевдонимПоказателя>,
	//	<ПсевдонимТаблицы>.Регистратор КАК Регистратор,
	//	<ПсевдонимТаблицы>.Период КАК Период,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, ДЕНЬ) КАК ПериодДень,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, НЕДЕЛЯ) КАК ПериодНеделя,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, ДЕКАДА) КАК ПериодДекада,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, МЕСЯЦ) КАК ПериодМесяц,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, КВАРТАЛ) КАК ПериодКвартал,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, ПОЛУГОДИЕ) КАК ПериодПолугодие,
	//	НАЧАЛОПЕРИОДА(<ПсевдонимТаблицы>.Период, ГОД) КАК ПериодГод
	//	//ПОЛЯ_СВОЙСТВА
	//	//ПОЛЯ_КАТЕГОРИИ
	//}
	//{УПОРЯДОЧИТЬ ПО
	//	<ПсевдонимПоля>.*,
	//	<ПсевдонимПоказателя>,
	//	Регистратор,
	//	Период,
	//	ПериодДень,
	//	ПериодНеделя,
	//	ПериодДекада,
	//	ПериодМесяц,
	//	ПериодКвартал,
	//	ПериодПолугодие,
	//	ПериодГод
	//	//УПОРЯДОЧИТЬ_СВОЙСТВА
	//	//УПОРЯДОЧИТЬ_КАТЕГОРИИ
	//}
	//ИТОГИ
	//	АГРЕГАТНАЯ_ФУНКЦИЯ(<ПсевдонимПоказателя>)
	//	//ИТОГИ_СВОЙСТВА
	//	//ИТОГИ_КАТЕГОРИИ
	//ПО
	//	ОБЩИЕ
	//{ИТОГИ ПО
	//	<ПсевдонимПоля>.*,
	//	Регистратор,
	//	Период,
	//	ПериодДень,
	//	ПериодНеделя,
	//	ПериодДекада,
	//	ПериодМесяц,
	//	ПериодКвартал,
	//	ПериодПолугодие,
	//	ПериодГод
	//	//ПОЛЯ_СВОЙСТВА
	//	//ПОЛЯ_КАТЕГОРИИ
	//}
	//АВТОУПОРЯДОЧИВАНИЕ
	
	
	//Илья потребочал, чтобы период затрат брался из даты регистратора
	
	
	ТекстЗапроса = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	ИсточникДанных.Подразделение КАК Подразделение,
	               |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Подразделение),
	               |	ИсточникДанных.СтатьяЗатрат КАК СтатьяЗатрат,
	               |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.СтатьяЗатрат),
	               |	ИсточникДанных.НоменклатурнаяГруппа КАК НоменклатурнаяГруппа,
	               |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.НоменклатурнаяГруппа),
	               |	ИсточникДанных.Заказ КАК Заказ,
	               |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Заказ),
	               |	ИсточникДанных.СуммаОборот КАК СуммаОборот,
	               |	ИсточникДанных.СуммаДолларОборот КАК СуммаДолларОборот,
	               |	ИсточникДанных.СуммаДолларТелемартОборот КАК СуммаДолларТелемартОборот,
	               |	ИсточникДанных.Регистратор КАК Регистратор,
	               |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Регистратор),
	               |	ИсточникДанных.Период КАК Период,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ДЕНЬ) КАК ПериодДень,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, НЕДЕЛЯ) КАК ПериодНеделя,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ДЕКАДА) КАК ПериодДекада,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, МЕСЯЦ) КАК ПериодМесяц,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, КВАРТАЛ) КАК ПериодКвартал,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ПОЛУГОДИЕ) КАК ПериодПолугодие,
	               |	НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ГОД) КАК ПериодГод
	               |{ВЫБРАТЬ
	               |	Подразделение.*,
	               |	СтатьяЗатрат.*,
	               |	НоменклатурнаяГруппа.*,
	               |	Заказ.*,
	               |	СуммаОборот,
	               |	СуммаДолларОборот,
	               |	СуммаДолларТелемартОборот,
	               |	Регистратор.* КАК Регистратор,
	               |	Период КАК Период,
	               |	ПериодДень,
	               |	ПериодНеделя,
	               |	ПериодДекада,
	               |	ПериодМесяц,
	               |	ПериодКвартал,
	               |	ПериодПолугодие,
	               |	ПериодГод}
	               |ИЗ
	               |	РегистрНакопления.ЗатратыКассовыйМетод.Обороты(&ДатаНач, &ДатаКон, Регистратор {(&Периодичность)}, {(Подразделение).* КАК Подразделение, (СтатьяЗатрат).* КАК СтатьяЗатрат, (НоменклатурнаяГруппа).* КАК НоменклатурнаяГруппа, (Заказ).* КАК Заказ}) КАК ИсточникДанных
	               |{ГДЕ
	               |	ИсточникДанных.СуммаОборот КАК СуммаОборот,
	               |	ИсточникДанных.СуммаДолларОборот КАК СуммаДолларОборот,
	               |	ИсточникДанных.СуммаДолларТелемартОборот КАК СуммаДолларТелемартОборот,
	               |	ИсточникДанных.Регистратор КАК Регистратор,
	               |	ИсточникДанных.Период КАК Период,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ДЕНЬ)) КАК ПериодДень,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, НЕДЕЛЯ)) КАК ПериодНеделя,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ДЕКАДА)) КАК ПериодДекада,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, МЕСЯЦ)) КАК ПериодМесяц,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, КВАРТАЛ)) КАК ПериодКвартал,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ПОЛУГОДИЕ)) КАК ПериодПолугодие,
	               |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Период, ГОД)) КАК ПериодГод}
	               |{УПОРЯДОЧИТЬ ПО
	               |	Подразделение.*,
	               |	СтатьяЗатрат.*,
	               |	НоменклатурнаяГруппа.*,
	               |	Заказ.*,
	               |	СуммаОборот,
	               |	СуммаДолларОборот,
	               |	СуммаДолларТелемартОборот,
	               |	Регистратор,
	               |	Период,
	               |	ПериодДень,
	               |	ПериодНеделя,
	               |	ПериодДекада,
	               |	ПериодМесяц,
	               |	ПериодКвартал,
	               |	ПериодПолугодие,
	               |	ПериодГод}
	               |ИТОГИ
	               |	СУММА(СуммаОборот),
	               |	СУММА(СуммаДолларОборот),
	               |	СУММА(СуммаДолларТелемартОборот)
	               |ПО
	               |	ОБЩИЕ
	               |{ИТОГИ ПО
	               |	Подразделение.*,
	               |	СтатьяЗатрат.*,
	               |	НоменклатурнаяГруппа.*,
	               |	Заказ.*,
	               |	Регистратор.*,
	               |	Период,
	               |	ПериодДень,
	               |	ПериодНеделя,
	               |	ПериодДекада,
	               |	ПериодМесяц,
	               |	ПериодКвартал,
	               |	ПериодПолугодие,
	               |	ПериодГод}";
				   
	//ТекстЗапроса = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	//			   |	ИсточникДанных.Подразделение КАК Подразделение,
	//			   |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Подразделение),
	//			   |	ИсточникДанных.СтатьяЗатрат КАК СтатьяЗатрат,
	//			   |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.СтатьяЗатрат),
	//			   |	ИсточникДанных.НоменклатурнаяГруппа КАК НоменклатурнаяГруппа,
	//			   |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.НоменклатурнаяГруппа),
	//			   |	ИсточникДанных.Заказ КАК Заказ,
	//			   |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Заказ),
	//			   |	ИсточникДанных.СуммаОборот КАК СуммаОборот,
	//			   |	ИсточникДанных.СуммаДолларОборот КАК СуммаДолларОборот,
	//			   |	ИсточникДанных.СуммаДолларТелемартОборот КАК СуммаДолларТелемартОборот,
	//			   |	ИсточникДанных.Регистратор КАК Регистратор,
	//			   |	ПРЕДСТАВЛЕНИЕ(ИсточникДанных.Регистратор),
	//			   |	ИсточникДанных.Регистратор.Дата КАК Период,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ДЕНЬ) КАК ПериодДень,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, НЕДЕЛЯ) КАК ПериодНеделя,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ДЕКАДА) КАК ПериодДекада,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, МЕСЯЦ) КАК ПериодМесяц,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, КВАРТАЛ) КАК ПериодКвартал,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ПОЛУГОДИЕ) КАК ПериодПолугодие,
	//			   |	НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ГОД) КАК ПериодГод
	//			   |{ВЫБРАТЬ
	//			   |	Подразделение.*,
	//			   |	СтатьяЗатрат.*,
	//			   |	НоменклатурнаяГруппа.*,
	//			   |	Заказ.*,
	//			   |	СуммаОборот,
	//			   |	СуммаДолларОборот,
	//			   |	СуммаДолларТелемартОборот,
	//			   |	Регистратор.* КАК Регистратор,
	//			   |	Период КАК Период,
	//			   |	ПериодДень,
	//			   |	ПериодНеделя,
	//			   |	ПериодДекада,
	//			   |	ПериодМесяц,
	//			   |	ПериодКвартал,
	//			   |	ПериодПолугодие,
	//			   |	ПериодГод}
	//			   |ИЗ
	//			   |	РегистрНакопления.Затраты.Обороты(, , Регистратор {(&Периодичность)}, {(Подразделение).* КАК Подразделение, (СтатьяЗатрат).* КАК СтатьяЗатрат, (НоменклатурнаяГруппа).* КАК НоменклатурнаяГруппа, (Заказ).* КАК Заказ}) КАК ИсточникДанных
	//			   |ГДЕ
	//			   |	ИсточникДанных.Регистратор.Дата МЕЖДУ &ДатаНач И &ДатаКон
	//			   |{ГДЕ
	//			   |	ИсточникДанных.СуммаОборот КАК СуммаОборот,
	//			   |	ИсточникДанных.СуммаДолларОборот КАК СуммаДолларОборот,
	//			   |	ИсточникДанных.СуммаДолларТелемартОборот КАК СуммаДолларТелемартОборот,
	//			   |	ИсточникДанных.Регистратор КАК Регистратор,
	//			   |	ИсточникДанных.Регистратор.Дата КАК Период,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ДЕНЬ)) КАК ПериодДень,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, НЕДЕЛЯ)) КАК ПериодНеделя,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ДЕКАДА)) КАК ПериодДекада,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, МЕСЯЦ)) КАК ПериодМесяц,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, КВАРТАЛ)) КАК ПериодКвартал,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ПОЛУГОДИЕ)) КАК ПериодПолугодие,
	//			   |	(НАЧАЛОПЕРИОДА(ИсточникДанных.Регистратор.Дата, ГОД)) КАК ПериодГод}
	//			   |{УПОРЯДОЧИТЬ ПО
	//			   |	Подразделение.*,
	//			   |	СтатьяЗатрат.*,
	//			   |	НоменклатурнаяГруппа.*,
	//			   |	Заказ.*,
	//			   |	СуммаОборот,
	//			   |	СуммаДолларОборот,
	//			   |	СуммаДолларТелемартОборот,
	//			   |	Регистратор,
	//			   |	Период,
	//			   |	ПериодДень,
	//			   |	ПериодНеделя,
	//			   |	ПериодДекада,
	//			   |	ПериодМесяц,
	//			   |	ПериодКвартал,
	//			   |	ПериодПолугодие,
	//			   |	ПериодГод}
	//			   |ИТОГИ
	//			   |	СУММА(СуммаОборот),
	//			   |	СУММА(СуммаДолларОборот),
	//			   |	СУММА(СуммаДолларТелемартОборот)
	//			   |ПО
	//			   |	ОБЩИЕ
	//			   |{ИТОГИ ПО
	//			   |	Подразделение.*,
	//			   |	СтатьяЗатрат.*,
	//			   |	НоменклатурнаяГруппа.*,
	//			   |	Заказ.*,
	//			   |	Регистратор.*,
	//			   |	Период,
	//			   |	ПериодДень,
	//			   |	ПериодНеделя,
	//			   |	ПериодДекада,
	//			   |	ПериодМесяц,
	//			   |	ПериодКвартал,
	//			   |	ПериодПолугодие,
	//			   |	ПериодГод}";			   
	
	// В универсальном отчете включен флаг использования свойств и категорий.
	Если УниверсальныйОтчет.ИспользоватьСвойстваИКатегории Тогда
		
		// Добавление свойств и категорий поля запроса в таблицу полей.
		// Необходимо вызывать для каждого поля запроса, предоставляющего возможность использования свойств и категорий.
		
		// УниверсальныйОтчет.ДобавитьСвойстваИКатегорииДляПоля(<ПсевдонимТаблицы>.<Поле> , <ПсевдонимПоля>, <Представление>, <Назначение>);
		УниверсальныйОтчет.ДобавитьСвойстваИКатегорииДляПоля( "ИсточникДанных.НоменклатурнаяГруппа" ,   "НоменклатурнаяГруппа",   "Номенклатурная группа",   ПланыВидовХарактеристик.НазначенияСвойствКатегорийОбъектов.Справочник_НоменклатурныеГруппы);

		// Добавление свойств и категорий в исходный текст запроса.
		УниверсальныйОтчет.ДобавитьВТекстЗапросаСвойстваИКатегории(ТекстЗапроса);
		
	КонецЕсли;
		
	// Инициализация текста запроса построителя отчета
	УниверсальныйОтчет.ПостроительОтчета.Текст = ТекстЗапроса;
	
	// Представления полей отчёта.
	// Необходимо вызывать для каждого поля запроса.
	// УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить(<ИмяПоля>, <ПредставлениеПоля>);
	УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить( "СтатьяЗатрат",				"Статья затрат");	
	УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить( "НоменклатурнаяГруппа",		"Номенклатурная группа");
	УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить( "СуммаОборот",				"Сумма");
	УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить( "СуммаДолларОборот",			"Сумма доллар");
    УниверсальныйОтчет.мСтруктураПредставлениеПолей.Вставить( "СуммаДолларТелемартОборот",	"Сумма доллар Телемарт");
	
	// Добавление показателей
	// Необходимо вызывать для каждого добавляемого показателя.
	// УниверсальныйОтчет.ДобавитьПоказатель(<ИмяПоказателя>, <ПредставлениеПоказателя>, <ВключенПоУмолчанию>, <Формат>, <ИмяГруппы>, <ПредставлениеГруппы>);
	УниверсальныйОтчет.ДобавитьПоказатель("СуммаОборот", "Сумма", Истина, "ЧЦ=15; ЧДЦ=2");
	УниверсальныйОтчет.ДобавитьПоказатель("СуммаДолларОборот", "Сумма доллар", Истина, "ЧЦ=15; ЧДЦ=2");
    УниверсальныйОтчет.ДобавитьПоказатель("СуммаДолларТелемартОборот", "Сумма доллар Телемарт", Истина, "ЧЦ=15; ЧДЦ=2");
	
	// Добавление предопредедлённых группировок строк отчёта.
	// Необходимо вызывать для каждой добавляемой группировки строки.
	// УниверсальныйОтчет.ДобавитьИзмерениеСтроки(<ПутьКДанным>);	
	УниверсальныйОтчет.ДобавитьИзмерениеСтроки("Подразделение");	
	УниверсальныйОтчет.ДобавитьИзмерениеСтроки("СтатьяЗатрат");
	 		
	// Добавление предопредедлённых группировок колонок отчёта.
	// Необходимо вызывать для каждой добавляемой группировки колонки.
	// УниверсальныйОтчет.ДобавитьИзмерениеКолонки(<ПутьКДанным>);
	
	// Добавление предопредедлённых отборов отчёта.
	// Необходимо вызывать для каждого добавляемого отбора.
	// УниверсальныйОтчет.ДобавитьОтбор(<ПутьКДанным>);
	// УниверсальныйОтчет.ДобавитьОтбор("Заказ");
	УниверсальныйОтчет.ДобавитьОтбор("ПодразделениеВладелец");
	УниверсальныйОтчет.ДобавитьОтбор("НоменклатурнаяГруппа");
	УниверсальныйОтчет.ДобавитьОтбор("Подразделение");
	УниверсальныйОтчет.ДобавитьОтбор("СтатьяЗатрат");
	
	// Установка связи подчинённых и родительских полей
	// УниверсальныйОтчет.УстановитьСвязьПолей(<ПутьКДанным>, <ПутьКДаннымРодитель>);
	
	// Установка представлений полей
	УниверсальныйОтчет.УстановитьПредставленияПолей(УниверсальныйОтчет.мСтруктураПредставлениеПолей, УниверсальныйОтчет.ПостроительОтчета);
	
	// Установка типов значений свойств в отборах отчёта
	УниверсальныйОтчет.УстановитьТипыЗначенийСвойствДляОтбора();
	
	// Заполнение начальных настроек универсального отчёта
	УниверсальныйОтчет.УстановитьНачальныеНастройки(Ложь);
	
	// Добавление дополнительных полей
	// Необходимо вызывать для каждого добавляемого дополнительного поля.
	УниверсальныйОтчет.ДобавитьДополнительноеПоле("Затрата.ЕдиницаХраненияОстатков");
	
КонецПроцедуры // УстановитьНачальныеНастройки()

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ФОРМИРОВАНИЯ ОТЧЁТА 
	
// Процедура формирования отчёта
//
Процедура СформироватьОтчет(ТабличныйДокумент) Экспорт
	
	// Перед формирование отчёта можно установить необходимые параметры универсального отчёта.
	
	УниверсальныйОтчет.СформироватьОтчет(ТабличныйДокумент);

КонецПроцедуры // СформироватьОтчет()

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

// Процедура обработки расшифровки
//
Процедура ОбработкаРасшифровки(Расшифровка, Объект) Экспорт
	
	// Дополнительные параметры в расшифровывающий отчет можно передать
	// посредством инициализации переменной "ДополнительныеПараметры".
	
	ДополнительныеПараметры = УправлениеОтчетами.СохранитьРеквизитыОтчета(ЭтотОбъект);
	УниверсальныйОтчет.ОбработкаРасшифровкиУниверсальногоОтчета(Расшифровка, Объект, ДополнительныеПараметры);
	
КонецПроцедуры // ОбработкаРасшифровки()

// Формирует структуру для сохранения настроек отчета
//
Процедура СформироватьСтруктуруДляСохраненияНастроек(СтруктураСНастройками) Экспорт
	
	УниверсальныйОтчет.СформироватьСтруктуруДляСохраненияНастроек(СтруктураСНастройками);
	УправлениеОтчетами.СохранитьРеквизитыОтчета(ЭтотОбъект, СтруктураСНастройками);
	
КонецПроцедуры // СформироватьСтруктуруДляСохраненияНастроек()

// Заполняет настройки отчета из структуры сохраненных настроек
//
Функция ВосстановитьНастройкиИзСтруктуры(СтруктураСНастройками) Экспорт
	
	УправлениеОтчетами.ВосстановитьРеквизитыОтчета(ЭтотОбъект, СтруктураСНастройками);
	Возврат УниверсальныйОтчет.ВосстановитьНастройкиИзСтруктуры(СтруктураСНастройками, ЭтотОбъект);
	
КонецФункции // ВосстановитьНастройкиИзСтруктуры()

// Содержит значение используемого режима ввода периода.
// Тип: Число.
// Возможные значения: 0 - произвольный период, 1 - на дату, 2 - неделя, 3 - декада, 4 - месяц, 5 - квартал, 6 - полугодие, 7 - год
// Значение по умолчанию: 0
// Пример:
// УниверсальныйОтчет.мРежимВводаПериода = 1;

#КонецЕсли
