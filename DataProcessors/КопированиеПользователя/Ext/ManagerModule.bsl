﻿
Процедура СоздатьНовогопользователяПоЭталонномуПользователю(НовыйПользователь, НашОтвет) Экспорт
	
	МассивОшибок = Новый Массив();
			
	//Проверка нового пользователя
	Если СокрЛП(НовыйПользователь.surname) = "" Тогда 	
		МассивОшибок.Добавить("Не указана фамилия нового пользователя. Отказ.");	
	КонецЕсли;	
	
	КраткоеИмя = ОбщиеСерверHTTP.СоздатьКраткоеИмя(НовыйПользователь.surname,
													НовыйПользователь.name,
													"");
																	
	Если Справочники.Пользователи.НайтиПоКоду(СокрЛП(КраткоеИмя)) <> Справочники.Пользователи.ПустаяСсылка() Тогда 
		МассивОшибок.Добавить("В базе 1с уже есть пользователь " + КраткоеИмя + " Отказ.");
	КонецЕсли;
	
	id_НовогоПользователя = СокрЛП(НовыйПользователь.id_employee);
		
	Если id_НовогоПользователя = "" Тогда 
		МассивОшибок.Добавить("Не указан код сайта нового пользователя " + КраткоеИмя + " Отказ.");
	КонецЕсли;
	
	Если Справочники.Пользователи.НайтиПоРеквизиту("КодСайта",id_НовогоПользователя) <> Справочники.Пользователи.ПустаяСсылка() Тогда
		МассивОшибок.Добавить("В базе 1с уже есть пользователь с таким кдом сайта " + id_НовогоПользователя + ". Отказ.");
	КонецЕсли;	
		
	ПолноеИмя = ОбщиеСерверHTTP.СоздатьПолноеИмя(НовыйПользователь.surname,
													НовыйПользователь.name,
													"");
																	
	Если МассивОшибок.Количество() = 0 Тогда 																
		МассивОшибок.Добавить(ОбщиеСерверHTTP.СформироватьПользователяИБ_и_Пользователя(НовыйПользователь.surname,
																						НовыйПользователь.name,
																						"",
																						КраткоеИмя,
																						ПолноеИмя,
																						Справочники.Пользователи.ЭталонныйПользователь,
																						СокрЛП(НовыйПользователь.password),
																						id_НовогоПользователя));
	КонецЕсли;
																									
	Для Каждого СтрокаСообщения из МассивОшибок Цикл		
		НашОтвет = СокрЛП(НашОтвет) + СтрокаСообщения;		
	КонецЦикла;
	
	НашОтвет = СокрЛП(НашОтвет);
	
КонецПроцедуры