﻿
//Если результат больше нуля - товарный кредит превышен
Функция ПолучитьДолгКлиентаНаКонецДня(	Контрагент,
										Дата,
										СуммаДляРасчётаДолга,
										КодВалютыДоговора,
										Организация = Неопределено) Экспорт
	
	Запрос = Новый Запрос("ВЫБРАТЬ РАЗРЕШЕННЫЕ
	                      |	СУММА(ВЫБОР
	                      |			КОГДА ВзаиморасчетыСКонтрагентамиОстатки.ДоговорКонтрагента.ВалютаВзаиморасчетов.Код = ""840""
	                      |				ТОГДА ЕСТЬNULL(ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток, 0)
	                      |			КОГДА КурсыВалютСрезПоследних.Курс = 0
	                      |				ТОГДА 0
	                      |			ИНАЧЕ ЕСТЬNULL(ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток, 0) * КурсыВалютСрезПоследних.КратностьТелемарт / КурсыВалютСрезПоследних.КурсТелемарт
	                      |		КОНЕЦ) КАК ОбщийДолгUSD
	                      |ПОМЕСТИТЬ СолянкаВТ
	                      |ИЗ
	                      |	РегистрНакопления.ВзаиморасчетыСКонтрагентами.Остатки(
	                      |			&Граница,
	                      |			Контрагент = &Контрагент
	                      |				И Организация = &Организация) КАК ВзаиморасчетыСКонтрагентамиОстатки
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КурсыВалют.СрезПоследних(&Дата, Валюта.Код = ""840"") КАК КурсыВалютСрезПоследних
	                      |		ПО (ИСТИНА)
	                      |ГДЕ
	                      |	ВзаиморасчетыСКонтрагентамиОстатки.ДоговорКонтрагента.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровКонтрагентов.СПокупателем)
	                      |
	                      |ОБЪЕДИНИТЬ ВСЕ
	                      |
	                      |ВЫБРАТЬ
	                      |	ВЫБОР
	                      |		КОГДА &ВалютаДоговораДокумента = ""840""
	                      |			ТОГДА &СуммаДокумента
	                      |		КОГДА КурсыВалютСрезПоследних.Курс = 0
	                      |			ТОГДА 0
	                      |		ИНАЧЕ &СуммаДокумента * КурсыВалютСрезПоследних.КратностьТелемарт / КурсыВалютСрезПоследних.КурсТелемарт
	                      |	КОНЕЦ - &ЛимитКонтрагента
	                      |ИЗ
	                      |	РегистрСведений.КурсыВалют.СрезПоследних(&Дата, Валюта.Код = ""840"") КАК КурсыВалютСрезПоследних
	                      |;
	                      |
	                      |////////////////////////////////////////////////////////////////////////////////
	                      |ВЫБРАТЬ
	                      |	СУММА(Солянка.ОбщийДолгUSD) КАК ОбщийДолгUSD
	                      |ИЗ
	                      |	СолянкаВТ КАК Солянка");
	
	Запрос.УстановитьПараметр("Организация", ?(Организация = Неопределено, Справочники.Организации.НайтиПоКоду("000000001"), Организация));
	Запрос.УстановитьПараметр("Граница", Новый Граница(КонецДня(Дата),ВидГраницы.Включая));
	Запрос.УстановитьПараметр("ЛимитКонтрагента", Контрагент.ОбщаяСуммаТоварногоКредита);
	Запрос.УстановитьПараметр("ВалютаДоговораДокумента", КодВалютыДоговора);		
	Запрос.УстановитьПараметр("СуммаДокумента",СуммаДляРасчётаДолга);	
	Запрос.УстановитьПараметр("Контрагент", Контрагент);		
	Запрос.УстановитьПараметр("Дата", НачалоДня(Дата));
	
	Результат = Запрос.Выполнить();
	
	Если Результат.Пустой() Тогда 
		Возврат 0;
	Иначе
		
		Выборка = Результат.Выбрать();
		Выборка.Следующий();
		
		Возврат Выборка.ОбщийДолгUSD;
		
	КонецЕсли;
	
КонецФункции

//Общая процедура проверки просрочки по алгоритму Ильи
Функция СформироватьМассивСообщенийПоПросрочке(Контрагент) Экспорт
	
	ПросрочкаДоллар = 0;
	
	МассивСообщениПоПросрочке = Новый Массив;
	
	ОтборДляДоговоров = Новый Структура("ВидДоговора",Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
	СправочникВыборка = Справочники.ДоговорыКонтрагентов.Выбрать(,Контрагент, ОтборДляДоговоров);
	Сегодня = КонецДня(ТекущаяДата());
	
	ЗапросПоПросрочке = Новый Запрос("ВЫБРАТЬ
	                                 |	СУММА(ВЫБОР
	                                 |			КОГДА ВзаиморасчетыСКонтрагентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	                                 |					И ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов > 0
	                                 |				ТОГДА ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов
	                                 |			КОГДА ВзаиморасчетыСКонтрагентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	                                 |					И ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов < 0
	                                 |				ТОГДА -ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов
	                                 |			ИНАЧЕ 0
	                                 |		КОНЕЦ) КАК Расход,
	                                 |	СУММА(ВЫБОР
	                                 |			КОГДА ВзаиморасчетыСКонтрагентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	                                 |					И ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов > 0
	                                 |				ТОГДА ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов
	                                 |			КОГДА ВзаиморасчетыСКонтрагентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	                                 |					И ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов < 0
	                                 |				ТОГДА -ВзаиморасчетыСКонтрагентами.СуммаВзаиморасчетов
	                                 |			ИНАЧЕ 0
	                                 |		КОНЕЦ) КАК Приход
	                                 |ПОМЕСТИТЬ ПравильныеДвиженияВТ
	                                 |ИЗ
	                                 |	РегистрНакопления.ВзаиморасчетыСКонтрагентами КАК ВзаиморасчетыСКонтрагентами
	                                 |ГДЕ
	                                 |	ВзаиморасчетыСКонтрагентами.ДоговорКонтрагента = &ДоговорКонтрагента
	                                 |	И ВзаиморасчетыСКонтрагентами.Период МЕЖДУ &ПервыйДеньКонтроля И &Сегодня
	                                 |;
	                                 |
	                                 |////////////////////////////////////////////////////////////////////////////////
	                                 |ВЫБРАТЬ
	                                 |	ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток - ЕСТЬNULL(ПравильныеДвиженияВТ.Расход, 0) КАК Просрочка,
	                                 |	ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток,
	                                 |	ПравильныеДвиженияВТ.Расход,
	                                 |	ПравильныеДвиженияВТ.Приход,
	                                 |	ВЫБОР
	                                 |		КОГДА ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток < ЕСТЬNULL(ПравильныеДвиженияВТ.Расход, 0)
	                                 |			ТОГДА 0
	                                 |		КОГДА &ВалютаДоговораДоллар
	                                 |			ТОГДА ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток - ЕСТЬNULL(ПравильныеДвиженияВТ.Расход, 0)
	                                 |		ИНАЧЕ (ВзаиморасчетыСКонтрагентамиОстатки.СуммаВзаиморасчетовОстаток - ЕСТЬNULL(ПравильныеДвиженияВТ.Расход, 0)) * КурсыВалютСрезПоследних.КратностьТелемарт / ВЫБОР
	                                 |				КОГДА КурсыВалютСрезПоследних.КурсТелемарт = 0
	                                 |					ТОГДА 1
	                                 |				ИНАЧЕ КурсыВалютСрезПоследних.КурсТелемарт
	                                 |			КОНЕЦ
	                                 |	КОНЕЦ КАК ПросрочкаДоллар
	                                 |ИЗ
	                                 |	РегистрНакопления.ВзаиморасчетыСКонтрагентами.Остатки(&ДеньКонтроляОстатков, ДоговорКонтрагента = &ДоговорКонтрагента) КАК ВзаиморасчетыСКонтрагентамиОстатки,
	                                 |	ПравильныеДвиженияВТ КАК ПравильныеДвиженияВТ,
	                                 |	РегистрСведений.КурсыВалют.СрезПоследних(&Сегодня, Валюта.Код = ""840"") КАК КурсыВалютСрезПоследних");
									 
	ЗапросПоПросрочке.УстановитьПараметр("Сегодня", Сегодня);								 
	
	Пока СправочникВыборка.Следующий() Цикл 
			
		Если СправочникВыборка.ДопустимоеЧислоДнейЗадолженности = 0 Тогда 
			Продолжить;
		КонецЕсли;	
		
		//ЗапросПоПросрочке.УстановитьПараметр("ПервыйДеньКонтроля", Началодня(Сегодня - (СправочникВыборка.ДопустимоеЧислоДнейЗадолженности + 5) * 24 * 3600));
		//ЗапросПоПросрочке.УстановитьПараметр("ДеньКонтроляОстатков", Сегодня - (СправочникВыборка.ДопустимоеЧислоДнейЗадолженности + 6) * 24 * 3600);
		//Задача 5112, было 5 дней, стал 1
		ЗапросПоПросрочке.УстановитьПараметр("ПервыйДеньКонтроля", Началодня(Сегодня - (СправочникВыборка.ДопустимоеЧислоДнейЗадолженности + 1) * 24 * 3600));
		ЗапросПоПросрочке.УстановитьПараметр("ДеньКонтроляОстатков", Сегодня - (СправочникВыборка.ДопустимоеЧислоДнейЗадолженности + 2) * 24 * 3600);		
		ЗапросПоПросрочке.УстановитьПараметр("ВалютаДоговораДоллар",?(СправочникВыборка.ВалютаВзаиморасчетов.Код = "840",Истина,Ложь));
		ЗапросПоПросрочке.УстановитьПараметр("ДоговорКонтрагента", СправочникВыборка.Ссылка);
				
		Результат = ЗапросПоПросрочке.Выполнить();
		
		Если не Результат.Пустой() Тогда 
			
			Выборка = Результат.Выбрать();
			
			Пока Выборка.Следующий() Цикл 				
				Если Выборка.Просрочка = Null тогда
					Продолжить;					
				ИначеЕсли Выборка.Просрочка > 0 Тогда
					
					МассивСообщениПоПросрочке.Добавить("По договору """ + СправочникВыборка.Ссылка + """ есть просроченный долг на " + Выборка.Просрочка + " " + СправочникВыборка.ВалютаВзаиморасчетов);
					ПросрочкаДоллар = ПросрочкаДоллар + Выборка.ПросрочкаДоллар;
					
				КонецЕсли;				
			КонецЦикла;
			
		КонецЕсли;	
				
	КонецЦикла;
	
	Если ПросрочкаДоллар < 50 и (Не (РольДоступна("КонтрольДебиторки") или РольДоступна("ПолныеПрава"))) Тогда 
		
		МассивСообщениПоПросрочке.Очистить();
		МассивСообщениПоПросрочке.Добавить("Общая просрочка по контрагенту " + ПросрочкаДоллар + " USD");
		
	КонецЕсли;	
			
	Возврат МассивСообщениПоПросрочке;
	
КонецФункции	
	