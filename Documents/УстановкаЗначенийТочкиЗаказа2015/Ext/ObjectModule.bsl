﻿Перем мИспользоватьХарактеристики Экспорт;
Перем мУдалятьДвижения;

///////////////////////////////////////////////
//	ПРОЦЕДУРЫ И ФУНКЦИИ ПРОВЕДЕНИЯ ДОКУМЕНТА

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	мУдалятьДвижения = НЕ ЭтоНовый();
	
	Для Каждого ТекстРока из Товары Цикл 		
		ТекстРока.СпособОпределенияЗначенияТочкиЗаказа = Перечисления.СпособыОпределенияЗначенияТочкиЗаказа.Фиксированная;		
	КонецЦикла;	
		
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Если мУдалятьДвижения Тогда		
		ОбщегоНазначения.УдалитьДвиженияРегистратора(ЭтотОбъект, Отказ);		
	КонецЕсли;

	// Сформируем структуру реквизитов шапки документа
	СтруктураШапкиДокумента = ОбщегоНазначения.СформироватьСтруктуруШапкиДокумента(ЭтотОбъект);

	// Заголовок для сообщений об ошибках проведения.
	Заголовок = ОбщегоНазначения.ПредставлениеДокументаПриПроведении(СтруктураШапкиДокумента);

	// Заполним по шапке документа дерево параметров, нужных при проведении.
	ДеревоПолейЗапросаПоШапке = ОбщегоНазначения.СформироватьДеревоПолейЗапросаПоШапке();
	
	// Сформируем запрос на дополнительные параметры, нужные при проведении, по данным шапки документа
	СтруктураШапкиДокумента = УправлениеЗапасами.СформироватьЗапросПоДеревуПолей(ЭтотОбъект, ДеревоПолейЗапросаПоШапке, СтруктураШапкиДокумента, "");

	ПроверитьЗаполнениеШапки(СтруктураШапкиДокумента, Отказ, Заголовок);
	
	СтруктураПолей = Новый Структура;
	
	СтруктураПолей.Вставить("Номенклатура", "Номенклатура");
	СтруктураПолей.Вставить("ХарактеристикаНоменклатуры", "ХарактеристикаНоменклатуры");
	СтруктураПолей.Вставить("Склад", "Склад");
	СтруктураПолей.Вставить("СпособОпределенияЗначенияТочкиЗаказа", "СпособОпределенияЗначенияТочкиЗаказа");
	СтруктураПолей.Вставить("ЗначениеТочкиЗаказа", "ЗначениеТочкиЗаказа * Коэффициент / Номенклатура.ЕдиницаХраненияОстатков.Коэффициент");
	СтруктураПолей.Вставить("МинимальныйСтраховойЗапас", "МинимальныйСтраховойЗапас * Коэффициент / Номенклатура.ЕдиницаХраненияОстатков.Коэффициент");
	СтруктураПолей.Вставить("ПроцентнаяСтавкаЗначенияТочкиЗаказа", "ПроцентнаяСтавкаЗначенияТочкиЗаказа");
	СтруктураПолей.Вставить("ПроцентнаяСтавкаМинимальногоСтраховогоЗапаса", "ПроцентнаяСтавкаМинимальногоСтраховогоЗапаса");
	СтруктураПолей.Вставить("ДатаНач", "ДатаНач");
	СтруктураПолей.Вставить("ДатаКон", "ДатаКон");
	
	РезультатЗапросаПоТоварам = ОбщегоНазначения.СформироватьЗапросПоТабличнойЧасти(ЭтотОбъект, "Товары", СтруктураПолей);
	
	//Исключаю строки с нулевыми значениями точки заказа
	ТаблицаПоТоварамСНулями = РезультатЗапросаПоТоварам.Выгрузить();
	
	ТаблицаПоТоварам = ТаблицаПоТоварамСНулями.СкопироватьКолонки();
		
	Для каждого ТекстРока из ТаблицаПоТоварамСНулями Цикл 
		Если ТекстРока.ЗначениеТочкиЗаказа <> 0 тогда
			
			НоваяСтрока = ТаблицаПоТоварам.Добавить();
			
			ЗаполнитьЗначенияСвойств(НоваяСтрока, ТекстРока);
			
		КонецЕсли;	
	КонецЦикла;
		
	ПроверитьЗаполнениеТабличнойЧастиТовары(ТаблицаПоТоварам, СтруктураШапкиДокумента, Отказ, Заголовок);

	Если Не Отказ Тогда		
		ДвиженияПоРегистрам(СтруктураШапкиДокумента, ТаблицаПоТоварам, Отказ, Заголовок);		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	ОбщегоНазначения.УдалитьДвиженияРегистратора(ЭтотОбъект, Отказ);
КонецПроцедуры

/////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ДЛЯ ОБЕСПЕЧЕНИЯ ПРОВЕДЕНИЯ ДОКУМЕНТА

// Проверяет правильность заполнения шапки документа.
// Если какой-то из реквизтов шапки, влияющий на проведение не заполнен или
// заполнен не корректно, то выставляется флаг отказа в проведении.
// Проверка выполняется по выборке из результата запроса по шапке,
// все проверяемые реквизиты должны быть включены в выборку по шапке.
//
// Параметры: 
//  ВыборкаПоШапкеДокумента	- выборка из результата запроса по шапке документа,
//  Отказ 					- флаг отказа в проведении.
//
Процедура ПроверитьЗаполнениеШапки(СтруктураШапкиДокумента, Отказ, Заголовок)

	// Укажем, что надо проверить:
	СтруктураОбязательныхПолей = Новый Структура();

	// Теперь вызовем общую процедуру проверки.
	ЗаполнениеДокументов.ПроверитьЗаполнениеШапкиДокумента(ЭтотОбъект, СтруктураОбязательныхПолей, Отказ, Заголовок);

КонецПроцедуры

Процедура ПроверитьЗаполнениеТабличнойЧастиТовары(ТаблицаПоТоварам, СтруктураШапкиДокумента, Отказ, Заголовок)
	Для каждого СтрокаТаблицы Из ТаблицаПоТоварам Цикл

		СтрокаНачалаСообщенияОбОшибке = "В строке номер """ + СокрЛП(СтрокаТаблицы.НомерСтроки) + """ ";

		// Номенклатура.
		Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Номенклатура) Тогда		
			ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнена Номенклатура.", Отказ, Заголовок);		
		КонецЕсли;

		// СпособОпределенияЗначенияТочкиЗаказа.
		Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.СпособОпределенияЗначенияТочкиЗаказа) Тогда		
			ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнен cпособ определения значения точки заказа.", Отказ, Заголовок);		
		Иначе
			
			Если СтрокаТаблицы.СпособОпределенияЗначенияТочкиЗаказа = Перечисления.СпособыОпределенияЗначенияТочкиЗаказа.Фиксированная Тогда
				
			// ЗначениеТочкиЗаказа.
			//	Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ЗначениеТочкиЗаказа) Тогда				
			//		ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение точки заказа.", Отказ, Заголовок);				
			//	КонецЕсли;
				
			ИначеЕсли СтрокаТаблицы.СпособОпределенияЗначенияТочкиЗаказа = Перечисления.СпособыОпределенияЗначенияТочкиЗаказа.СреднийРазмерПартии Тогда
				
				// ДатаНач.
				Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ДатаНач) Тогда				
					ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение даты начала периода.", Отказ, Заголовок);				
				КонецЕсли;
				
				// ДатаКон.
				Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ДатаКон) Тогда				
					ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение даты окончания периода.", Отказ, Заголовок);				
				КонецЕсли;
				
				// ПроцентнаяСтавкаЗначенияТочкиЗаказа.
				Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ПроцентнаяСтавкаЗначенияТочкиЗаказа) Тогда				
					ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение процентной ставка значения точки заказа.", Отказ, Заголовок);				
				КонецЕсли;
				
			ИначеЕсли СтрокаТаблицы.СпособОпределенияЗначенияТочкиЗаказа = Перечисления.СпособыОпределенияЗначенияТочкиЗаказа.ОптимальныйРазмерЗаказа Тогда
				
				// ДатаНач.
				Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ДатаНач) Тогда				
					ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение даты начала периода.", Отказ, Заголовок);				
				КонецЕсли;
				
				// ДатаКон.
				Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.ДатаКон) Тогда				
					ОбщегоНазначения.СообщитьОбОшибке(СтрокаНачалаСообщенияОбОшибке + "не заполнено значение даты окончания периода.", Отказ, Заголовок);				
				КонецЕсли;
				
			КонецЕсли;
			
		КонецЕсли;

	КонецЦикла;
КонецПроцедуры

Процедура ДвиженияПоРегистрам(СтруктураШапкиДокумента, ТаблицаПоТоварам, Отказ, Заголовок)
	
	НаборДвижений   = Движения.ЗначенияТочкиЗаказа;
	ТаблицаДвижений = НаборДвижений.ВыгрузитьКолонки();
	
	// Заполним таблицу движений.
	ОбщегоНазначения.ЗагрузитьВТаблицуЗначений(ТаблицаПоТоварам, ТаблицаДвижений);
	
	НаборДвижений.мПериод          = Дата;
	НаборДвижений.мТаблицаДвижений = ТаблицаДвижений;

	Если Не Отказ Тогда		
		Движения.ЗначенияТочкиЗаказа.ВыполнитьДвижения();		
	КонецЕсли;

КонецПроцедуры

	
мИспользоватьХарактеристики = глЗначениеПеременной("ИспользоватьХарактеристикиНоменклатуры");
