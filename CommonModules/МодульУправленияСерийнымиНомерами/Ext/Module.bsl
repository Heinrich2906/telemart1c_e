﻿//МКМ ЗАЯ 20101125
//Возвращает таблицу несоответствий
Функция ВыборкаНесоответствияСНиНоменклатуры(Источник) Экспорт
	
	метаданныеДок = Источник.Метаданные();
	
	запрос = Новый Запрос;
	запрос.УстановитьПараметр("РО", Источник.Ссылка);
	
	//Проверка на соответствие количества сн и номенклатуры
	запрос.Текст = "ВЫБРАТЬ
	               |	ВложенныйЗапрос.Номенклатура,
	               |	ВложенныйЗапрос.Количество,
	               |	ВложенныйЗапрос.КоличествоСН
	               |ИЗ
	               |	(ВЫБРАТЬ
	               |		Товары.Номенклатура КАК Номенклатура,
	               |		КОЛИЧЕСТВО(РАЗЛИЧНЫЕ СерийныеНомера.СерийныйНомер) КАК КоличествоСН,
	               |		Товары.Количество КАК Количество
	               |	ИЗ
	               |		Документ." + метаданныеДок.Имя + ".Товары КАК Товары
	               |			ЛЕВОЕ СОЕДИНЕНИЕ Документ." + метаданныеДок.Имя + ".СерийныеНомера КАК СерийныеНомера
	               |			ПО Товары.КлючСвязи = СерийныеНомера.КлючСвязи
	               |				И Товары.Ссылка = СерийныеНомера.Ссылка
	               |			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ДатыСтартаСканированияСерийныхНомеров КАК ДатыСтартаСканированияСерийныхНомеров
	               |			ПО Товары.Номенклатура.Производитель = ДатыСтартаСканированияСерийныхНомеров.Производитель
	               |				И Товары.Ссылка.Дата >= ДатыСтартаСканированияСерийныхНомеров.ДатаСтарта
	               |	ГДЕ
	               |		Товары.Ссылка = &РО
	               |		И Товары.Номенклатура.ВестиСерийныеНомера
	               |		И (НЕ ДатыСтартаСканированияСерийныхНомеров.ДатаСтарта ЕСТЬ NULL )
	               |	
	               |	СГРУППИРОВАТЬ ПО
	               |		Товары.Номенклатура,
	               |		Товары.Количество) КАК ВложенныйЗапрос
	               |ГДЕ
	               |	ВложенныйЗапрос.КоличествоСН <> ВложенныйЗапрос.Количество";
				     				   
	Возврат запрос.Выполнить();
	
КонецФункции	

//МКМ ЗАЯ 20101125
Функция ВидДвиженияПоДокументу(ИмяДокумента, Ссылка) Экспорт 
	
	Если ИмяДокумента = "ПоступлениеТоваровУслуг" ИЛИ  ИмяДокумента = "ВозвратТоваровПоставщику"  Тогда 
		Возврат ВидДвиженияНакопления.Приход;
	КонецЕсли;
	
	Возврат ВидДвиженияНакопления.Расход;
	
КонецФункции	

//МКМ ЗАЯ 20101125
Функция ЗнакУчетаСерийникаВПоводке(ИмяДокумента, Ссылка) Экспорт 
	
	Если  ИмяДокумента = "ВозвратТоваровПоставщику" ИЛИ ИмяДокумента = "ВозвратТоваровОтПокупателя" Тогда 
		Возврат -1;
	КонецЕсли;
	
	Возврат 1;
	
КонецФункции

//МКМ ЗАЯ 20101125  
Функция ПоискШтрихкода(Штрихкод) Экспорт 
	
	запрос = Новый Запрос("ВЫБРАТЬ
	                      |	Штрихкоды.Штрихкод,
	                      |	Штрихкоды.Владелец,
	                      |	Штрихкоды.ТипШтрихкода,
	                      |	Штрихкоды.ЕдиницаИзмерения,
	                      |	Штрихкоды.ХарактеристикаНоменклатуры,
	                      |	Штрихкоды.СерияНоменклатуры,
	                      |	Штрихкоды.Качество
	                      |ИЗ
	                      |	РегистрСведений.Штрихкоды КАК Штрихкоды
	                      |ГДЕ
	                      |	Штрихкоды.Штрихкод = &Штрихкод");
				   
	запрос.УстановитьПараметр("Штрихкод", Штрихкод);
	
	Возврат запрос.Выполнить();
	
КонецФункции

//МКМ ЗАЯ 20101125 
Процедура СерийныйНомерОкончаниеВводаТекста(Текст, ЭлементыФормы, КлючСвязи, срНоменклатура, СерийныеНомера, ЭтаФорма) Экспорт
	
	Если Не ЗначениеЗаполнено(СокрЛП(Текст)) Тогда 
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(срНоменклатура) Или Не ЗначениеЗаполнено(КлючСвязи) Тогда 
		Сообщить("Сосканируйте штрихкод!!!", СтатусСообщения.Важное);
		Возврат;
	КонецЕсли;	
	
	серийник = Справочники.СерийныеНомера.НайтиПоКоду(СокрЛП(Текст),,,срНоменклатура);
	
	ТипДокумента = ТипЗнч(ЭтаФорма.ДокументОбъект.Ссылка);
	
	Если  ТипДокумента = Тип("ДокументСсылка.РеализацияТоваровУслуг") И серийник = Справочники.СерийныеНомера.ПустаяСсылка() Тогда
		
		#Если Клиент Тогда
			Предупреждение("Серийный номер " + СокрЛП(Текст) + " не найден в базе!");		
		#КонецЕсли
		
		Возврат;
		
	КонецЕсли;

	Если Не ЗначениеЗаполнено(серийник) Тогда 
		
		серийник = Справочники.СерийныеНомера.СоздатьЭлемент();
		серийник.Владелец = срНоменклатура;
		серийник.Код = СокрЛП(Текст);
		
		Попытка
			
			серийник.Записать();
			серийник = серийник.Ссылка;
			
		Исключение
			#Если Клиент Тогда
				Сообщить("Не удалось записать серийный номер, повторите попытку!!!", СтатусСообщения.Важное);
			#КонецЕсли	
		КонецПопытки;	
		
	Иначе 	
		
		Строка = СерийныеНомера.Найти(серийник, "СерийныйНомер");
		
		Если Строка <> Неопределено Тогда
			#Если Клиент Тогда
				Предупреждение("Серийный номер " + СокрЛП(Текст) + " уже был указан в документе!");
			#КонецЕсли
			
			Возврат;
			
		КонецЕсли;
		
	КонецЕсли;	
	
	стр = СерийныеНомера.Добавить();
	стр.СерийныйНомер = серийник;
	стр.КлючСвязи = КлючСвязи;
	
	#Если Клиент Тогда
		
		ЭлементыФормы.Штрихкод.Значение = "";		
		ЭлементыФормы.СерийныйНомер.Значение = "";
		ЭтаФорма.ТекущийЭлемент = ЭлементыФормы.Штрихкод;
		ЭлементыФормы.СерийныйНомер.Доступность = Ложь;
		
	#КонецЕсли	
		
	КлючСвязи = 0;
	
	срНоменклатура = Справочники.Номенклатура.ПустаяСсылка();
	
КонецПроцедуры

//МКМ ЗАЯ 20101125
Процедура ШтрихкодОкончаниеВводаТекста(Текст, ЭлементыФормы, КлючСвязи, срНоменклатура, Товары, ЭтаФорма, мПараметрыСвязиСтрокТЧ, Ссылка) Экспорт
	
	запрос = Новый Запрос("ВЫБРАТЬ
	                      |	Штрихкоды.Штрихкод,
	                      |	Штрихкоды.Владелец,
	                      |	Штрихкоды.ТипШтрихкода,
	                      |	Штрихкоды.ЕдиницаИзмерения,
	                      |	Штрихкоды.ХарактеристикаНоменклатуры,
	                      |	Штрихкоды.СерияНоменклатуры,
	                      |	Штрихкоды.Качество
	                      |ИЗ
	                      |	РегистрСведений.Штрихкоды КАК Штрихкоды
	                      |ГДЕ
	                      |	Штрихкоды.Владелец В(&Владелец)
	                      |	И Штрихкоды.Штрихкод В(&Штрихкод)
	                      |	И Штрихкоды.Владелец.ВестиСерийныеНомера
	                      |	И Штрихкоды.Владелец ССЫЛКА Справочник.Номенклатура");
				   
	запрос.УстановитьПараметр("Владелец", Товары.ВыгрузитьКолонку("Номенклатура"));	
	запрос.УстановитьПараметр("Штрихкод", СокрЛП(ЭлементыФормы.Штрихкод.Значение));
	
	выборка = запрос.Выполнить().Выбрать();
	
	Пока  выборка.Следующий() Цикл  
		
			стр = Товары.Найти(выборка.Владелец, "Номенклатура");
			
			Если стр = Неопределено Тогда 
				Продолжить;
			КонецЕсли;	
			
			срНоменклатура = выборка.Владелец;
			#Если Клиент Тогда
				ЭлементыФормы.Товары.ТекущаяСтрока = стр;
			#КонецЕсли
			
			Если стр.КлючСвязи = 0 Тогда
				стр.КлючСвязи = УчетСерийныхНомеров.ПолучитьНовыйКлючСвязи(мПараметрыСвязиСтрокТЧ, Ссылка, "Товары", Истина);
			КонецЕсли;
			
			КлючСвязи = стр.КлючСвязи;
			
			#Если Клиент Тогда
				
				ЭтаФорма.ТекущийЭлемент = ЭлементыФормы.СерийныйНомер;
				ЭлементыФормы.СерийныйНомер.Доступность = Истина;
				
			#КонецЕсли
			
			Возврат;
			
	КонецЦикла;
	
	Сообщить("Номенклатуры с таким штрихкодом в списке товаров нет или по ней не ведётся учет серийных номеров!", СтатусСообщения.Важное);
	
	#Если Клиент Тогда
		
		ЭлементыФормы.Штрихкод.Значение = "";
		ЭтаФорма.ТекущийЭлемент = ЭлементыФормы.Штрихкод;
		ЭлементыФормы.СерийныйНомер.Доступность = Ложь;
		
	#КонецЕсли	
		
	КлючСвязи = 0;
	
	срНоменклатура = Справочники.Номенклатура.ПустаяСсылка();

КонецПроцедуры

//МКМ ЗАЯ 20101125
Функция ПроверкаНаКорректностьДлины(СН) Экспорт 	
	
	Если СтрДлина(СН) = Константы.ДлинаСН.Получить() Тогда 
		Возврат Истина;
	КонецЕсли;	
	
	Возврат Ложь;
	
КонецФункции

//МКМ ЗАЯ 20101125 правила разбора кодов по производителям
Функция  ОбработатьШтрихКодБезПроизводителя(Код) Экспорт 
	
	Код = СокрЛП(Код);
	Длина = СтрДлина(Код);
	
	запрос = Новый Запрос("ВЫБРАТЬ
	               |	ПравилаШтрихкодовПроизводителей.Производитель,
	               |	ПравилаШтрихкодовПроизводителей.КоличествоСимволовВКоде,
	               |	ПравилаШтрихкодовПроизводителей.КоличествоЗначимыхСимволов
	               |ИЗ
	               |	РегистрСведений.ПравилаШтрихкодовПроизводителей КАК ПравилаШтрихкодовПроизводителей
	               |ГДЕ
	               |	ПравилаШтрихкодовПроизводителей.КоличествоСимволовВКоде = &КоличествоСимволовВКоде");
				   
	запрос.УстановитьПараметр("КоличествоСимволовВКоде", Длина);	
	
	рез = запрос.Выполнить().Выбрать();
	
	штрихкоды = Новый Массив;
	штрихкоды.Добавить(Код);
	
	Пока  рез.Следующий() Цикл 
		штрихкоды.Добавить(Лев(Код, рез.КоличествоЗначимыхСимволов));
	КонецЦикла;
		
	Возврат штрихкоды;	
	
КонецФункции

//МКМ ЗАЯ 20101125
Функция СерийникиЗаполнены(Док) Экспорт 
	
	Для Каждого стр Из Док.Товары Цикл 
		Если стр.Номенклатура.ВестиСерийныеНомера Тогда
			
			ОтборПроизводителя = Новый Структура;
			ОтборПроизводителя.Вставить("Производитель", стр.Номенклатура.Производитель);
			
			датаСтарта = РегистрыСведений.ДатыСтартаСканированияСерийныхНомеров.Выбрать(ОтборПроизводителя);
			
			Если датаСтарта.Следующий() Тогда 
				Если Док.Дата >= датаСтарта.ДатаСтарта Тогда 
					
					ПараметрыОтбора = Новый Структура;
					ПараметрыОтбора.Вставить("КлючСвязи", стр.КлючСвязи);
					строки = Док.СерийныеНомера.НайтиСтроки(ПараметрыОтбора);
					
					Если строки.Количество() <> стр.Количество Тогда 
						Возврат Ложь;
					КонецЕсли;	
					
				КонецЕсли;
			КонецЕсли;	
		КонецЕсли;	
	КонецЦикла;
	
	Возврат Истина;
	
КонецФункции 	

//МКМ ЗАЯ 20101125 Процедура - обработчик события "Нажатие" кнопки "ПоискПоШтрихКоду" в ТЧ.
//
Процедура ПоискПоШтрихКоду(Док) Экспорт 

	метаданныеДок = Док.Метаданные();
	
	#Если Клиент Тогда
		Результат = РаботаСТорговымОборудованием.ВвестиШтрихкод();
	#КонецЕсли
	
	Результат = СокрЛП(Результат);
	
	Если Не ПустаяСтрока(Результат) Тогда
		
		Результат = ИнвертированиеБуквАлфавитаИзЛатинициВКирилицу(Результат);
		
		запрос = Новый Запрос();
		запрос.Текст = "ВЫБРАТЬ
		               |	Док.Ссылка
		               |ИЗ
		               |	Документ." + метаданныеДок.Имя + " КАК Док
		               |ГДЕ
		               |	Док.Номер = &Номер";
					   
		запрос.УстановитьПараметр("Номер",Результат);			   
		рез = запрос.Выполнить().Выбрать();
		
		Если рез.Следующий() Тогда 
			рез.Ссылка.ПолучитьФорму("ФормаДокумента").Открыть();
		Иначе
			#Если Клиент Тогда
				Предупреждение("Документ не найден!!!");
			#КонецЕсли
		КонецЕсли;	
		
	КонецЕсли;

КонецПроцедуры // КоманднаяПанельТоварыПоискПоШтрихКоду()

//МКМ ЗАЯ 20101125
#Если Клиент Тогда
Процедура ВыводКоличестваОтсканированыхСерийников(ДанныеСтроки, ОформлениеСтроки, СерийныеНомера) Экспорт 
	
	Если ЗначениеЗаполнено(ДанныеСтроки.КлючСвязи) Тогда 
		
		ПараметрыОтбора = Новый Структура;
		ПараметрыОтбора.Вставить("КлючСвязи", ДанныеСтроки.КлючСвязи);		
		строки = СерийныеНомера.НайтиСтроки(ПараметрыОтбора);
		ОформлениеСтроки.Ячейки.Отсканировано.Значение = строки.Количество();
		
	ИначеЕсли ДанныеСтроки.Номенклатура.ВестиСерийныеНомера Тогда 
		ОформлениеСтроки.Ячейки.Отсканировано.Значение = 0;
	КонецЕсли;	

КонецПроцедуры	
#КонецЕсли

//МКМ ЗАЯ 20101125
Процедура КонтрольОстанковПоСН(Источник, Отказ, РежимПроведения) Экспорт 
	
	метаданныеДок = Источник.Метаданные();
	
	запрос = Новый Запрос("ВЫБРАТЬ
		               	  |	СерийныеНомера.Ссылка,
		               	  |	СерийныеНомера.НомерСтроки,
		               	  |	СерийныеНомера.КлючСвязи,
		               	  |	СерийныеНомера.СерийныйНомер,
		               	  |	Товары.Номенклатура
		               	  |ИЗ
		               	  |	Документ." + метаданныеДок.Имя + ".СерийныеНомера КАК СерийныеНомера
		               	  |		ЛЕВОЕ СОЕДИНЕНИЕ Документ." + метаданныеДок.Имя + ".Товары КАК Товары
		               	  |		ПО СерийныеНомера.КлючСвязи = Товары.КлючСвязи
		               	  |			И (Товары.Ссылка = &Док)
		               	  |ГДЕ
		               	  |	СерийныеНомера.Ссылка = &Док");
					   
	запрос.УстановитьПараметр("Док", Источник.Ссылка);				   
					   
	СерийныеНомера = запрос.Выполнить().Выгрузить();			   
	
	остатокПоСН = 0;
	
	Если метаданныеДок.Имя = "ВозвратТоваровПоставщику"
	 //ИЛИ метаданныеДок.Имя = "ВозвратТоваровОтПокупателя"
	 ИЛИ метаданныеДок.Имя = "РеализацияТоваровУслуг" Тогда 
		остатокПоСН = 1;
	КонецЕсли;	
	
	Для Каждого сн из СерийныеНомера Цикл 
		
		РеальныйОстатокСН = ПолучитьОстатокПоСн(Источник.Дата, сн.Номенклатура, сн.СерийныйНомер);
		
		Если РеальныйОстатокСН = 0  Тогда 
			Если остатокПоСН <> 0 Тогда 
				
				#Если Клиент Тогда
					Если сн.Номенклатура.ВестиСерийныеНомера Тогда 
						Сообщить("Не хватает в остатке СН: " + СокрЛП(сн.СерийныйНомер) + " по номенклатуре: " +  сн.Номенклатура, СтатусСообщения.Важное);
					КонецЕсли;
				#КонецЕсли

			КонецЕсли;	
		Иначе 	
			Если остатокПоСН = 0 Тогда 
				Если РеальныйОстатокСН > 0 Тогда 
					
					Если метаданныеДок.Имя <> "ВозвратТоваровОтПокупателя" тогда
						Отказ = (РежимПроведения = РежимПроведенияДокумента.Оперативный);
					КонецЕсли;	
					
					#Если Клиент Тогда
						Сообщить("Уже есть на балансе СН: " + СокрЛП(сн.СерийныйНомер) + " по номенклатуре: " +  сн.Номенклатура, СтатусСообщения.Важное);
					#КонецЕсли

				Иначе 	
					
					Если метаданныеДок.Имя <> "ВозвратТоваровОтПокупателя" тогда
						Отказ = (РежимПроведения = РежимПроведенияДокумента.Оперативный);
					КонецЕсли;	
					
					#Если Клиент Тогда
						Сообщить("На балансе минус по СН: " + СокрЛП(сн.СерийныйНомер) + " по номенклатуре: " +  сн.Номенклатура, СтатусСообщения.Важное);
					#КонецЕсли

				КонецЕсли;	
			КонецЕсли;	
		КонецЕсли;	
		
	КонецЦикла;	
	
КонецПроцедуры

//МКМ ЗАЯ 20101125
Функция ПолучитьОстатокПоСн(Дата, Номенклатура, СН) Экспорт 
	
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	СерийныеНомераОстатки.Номенклатура,
	                      |	СерийныеНомераОстатки.СерийныйНомер,
	                      |	СерийныеНомераОстатки.КоличествоОстаток
	                      |ИЗ
	                      |	РегистрНакопления.СерийныеНомера.Остатки(
	                      |			&Дата,
	                      |			Номенклатура = &Номенклатура
	                      |				И СерийныйНомер = &СерийныйНомер) КАК СерийныеНомераОстатки");
	 
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	Запрос.УстановитьПараметр("СерийныйНомер", СокрЛП(СН));
	Запрос.УстановитьПараметр("Дата", Дата);
	 
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	 
	Пока Выборка.Следующий() Цикл
		Возврат Выборка.КоличествоОстаток;
	КонецЦикла;
	 
	Возврат 0;
	 
КонецФункции	

Функция ИнвертированиеБуквАлфавитаИзЛатинициВКирилицу(Номер) экспорт

	КирилицаС = 1039;
	КирилицаПо = 1066;
	
	ЛатиницаС = 64;
	ЛатиницаПо = 91;
	
	Номер = ВРег(Номер);
	
	Для х = 1 По  СтрДлина(Номер) Цикл 
		
		код = КодСимвола(Номер, х); 
		
		Если код < ЛатиницаПо И ЛатиницаС < код Тогда 
			
			кодЛатиница = КирилицаС + (код - ЛатиницаС);
			Номер = СтрЗаменить(Номер, Сред(Номер, х, 1), Символ(кодЛатиница));
			
		КонецЕсли;	
		
	КонецЦикла;	
	
	Возврат Номер;

КонецФункции