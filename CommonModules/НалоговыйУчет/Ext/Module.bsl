﻿//Возвращает схему налогообложения Организации на Дату
Функция ПолучитьСхемуНалогообложения(Дата,Организация) Экспорт
	
	УчетнаяПолитикаНУ = РегистрыСведений.УчетнаяПолитикаНалоговыйУчет.ПолучитьПоследнее(Дата, Новый Структура("Организация", Организация));
	Если ЗначениеЗаполнено(УчетнаяПолитикаНУ.СхемаНалогообложения) Тогда
		Возврат УчетнаяПолитикаНУ.СхемаНалогообложения;
	Иначе
		Возврат Справочники.СхемыНалогообложения.НеПлательщик;
	КонецЕсли; 
	
КонецФункции // ПолучитьСхемуНалогообложения()

Функция НоваяСхемаНДС(ДатаДок) Экспорт
	
	Если ДатаДок < '20050729' Тогда
		Возврат Ложь;
	Иначе
		Возврат Истина;
	КонецЕсли;	
	
КонецФункции	
                 
// Формирует движения по регистрам НДСПриобретений и НДСПродаж платежных докуменов
Процедура ДвиженияПоРегистрамНДС(СтруктураПараметров, СтруктураДвижений, ВалютаРегламентированногоУчета, РежимПроведения=Неопределено, Объект=Null, Отказ=Ложь) Экспорт
	
	НаборДвиженийНДСПриобретений = СтруктураДвижений.НДСПриобретений; 	
	НаборДвиженийНДСПродаж       = СтруктураДвижений.НДСПродаж; 	

	Организация             = СтруктураПараметров.Организация;
	НаправлениеДвижения     = СтруктураПараметров.НаправлениеДвижения;
	РасчетыВозврат          = СтруктураПараметров.РасчетыВозврат;
	
	//Получение основных данных документа
	РеестрПлатежей = СтруктураПараметров.Таблица;
	
	// движения по регистрам НДСПриобретений и НДСПродаж
	Если  НаправлениеДвижения = Перечисления.НаправленияДвижений.Поступление И РасчетыВозврат = Перечисления.РасчетыВозврат.Расчеты 
		  ИЛИ НаправлениеДвижения = Перечисления.НаправленияДвижений.Выбытие     И РасчетыВозврат = Перечисления.РасчетыВозврат.Возврат Тогда
		ТаблицаНДСПродаж = РеестрПлатежей.Скопировать();
		ТаблицаНДСПродаж.Свернуть("ДоговорКонтрагента,ВозвратнаяТара", "СуммаСНДСРегл");
		ТаблицаНДСПродаж.Колонки.СуммаСНДСРегл.Имя = "СуммаВзаиморасчетовРегл";
				
		ТаблицаДвижений = НаборДвиженийНДСПродаж.Выгрузить();
				
		ТаблицаДвижений.Очистить();
		ОбщегоНазначения.ЗагрузитьВТаблицуЗначений(ТаблицаНДСПродаж, ТаблицаДвижений);
				
		Если РасчетыВозврат = Перечисления.РасчетыВозврат.Расчеты Тогда
			СобытиеНДС = Перечисления.СобытияНДСПродаж.ОплатаПокупателем;
		Иначе
			СобытиеНДС = Перечисления.СобытияНДСПродаж.ВозвратОплатыПокупателю;
		КонецЕсли;
		ТаблицаДвижений.ЗаполнитьЗначения(Организация , "Организация");
		ТаблицаДвижений.ЗаполнитьЗначения(СобытиеНДС  , "СобытиеНДС");
				
		Если НЕ Отказ И ТаблицаДвижений.Количество() > 0 Тогда
			НаборДвиженийНДСПродаж.мПериод          = СтруктураПараметров.Период;
			НаборДвиженийНДСПродаж.мТаблицаДвижений = ТаблицаДвижений;

			НаборДвиженийНДСПродаж.ВыполнитьПриход();
		КонецЕсли;
				
	ИначеЕсли НаправлениеДвижения = Перечисления.НаправленияДвижений.Выбытие     И РасчетыВозврат = Перечисления.РасчетыВозврат.Расчеты 
		  ИЛИ НаправлениеДвижения = Перечисления.НаправленияДвижений.Поступление И РасчетыВозврат = Перечисления.РасчетыВозврат.Возврат Тогда
		ТаблицаНДСПриобретений = РеестрПлатежей.Скопировать();
		ТаблицаНДСПриобретений.Свернуть("ДоговорКонтрагента,ВозвратнаяТара", "СуммаСНДСРегл");
		ТаблицаНДСПриобретений.Колонки.СуммаСНДСРегл.Имя = "СуммаВзаиморасчетовРегл";
			
		ТаблицаДвижений = НаборДвиженийНДСПриобретений.Выгрузить();
				
		ТаблицаДвижений.Очистить();
		ОбщегоНазначения.ЗагрузитьВТаблицуЗначений(ТаблицаНДСПриобретений, ТаблицаДвижений);
				
		Если РасчетыВозврат = Перечисления.РасчетыВозврат.Расчеты Тогда
			СобытиеНДС = Перечисления.СобытияНДСПриобретений.ОплатаПоставщику;
		Иначе
			СобытиеНДС = Перечисления.СобытияНДСПриобретений.ВозвратОплатыПоставщиком;
		КонецЕсли;
		ТаблицаДвижений.ЗаполнитьЗначения(Организация , "Организация");
		ТаблицаДвижений.ЗаполнитьЗначения(СобытиеНДС  , "СобытиеНДС");
		
		// при импорте учет НДС Приобретений не ведем, так как налоговый кредит возникает
		// в момент погашения налогового обязательства (оформления ГТД) на таможне, а не 
		// при оплате/поступлении.
		Сч = 0;
		Пока ТаблицаДвижений.Количество() > Сч  Цикл
			СтрокаТаблицы = ТаблицаДвижений[Сч];
			Если СтрокаТаблицы.ДоговорКонтрагента.Внешнеэкономический Тогда
				ТаблицаДвижений.Удалить(СтрокаТаблицы);
			Иначе
				Сч = Сч + 1;
			КонецЕсли;
		КонецЦикла; 
		
		Если НЕ Отказ И ТаблицаДвижений.Количество() > 0 Тогда
			НаборДвиженийНДСПриобретений.мПериод          = СтруктураПараметров.Период;
			НаборДвиженийНДСПриобретений.мТаблицаДвижений = ТаблицаДвижений;

			НаборДвиженийНДСПриобретений.ВыполнитьПриход();
		КонецЕсли;
					
	КонецЕсли;
		
КонецПроцедуры

//Формирует структуру параметров для передачи в процедуру ДвиженияПоРегистрамНДС
Функция ПодготовкаСтруктурыПараметровДляНДС(Ссылка, ВалютаРегламентированногоУчета, Заголовок = Неопределено) Экспорт

	ВидДокумента = Ссылка.Метаданные().Имя;
	РеквизитыДокумента = Ссылка.Метаданные().Реквизиты;

	
	НаправлениеДвижения = ОпределениеНаправленияДвиженияДляДокументаДвиженияДенежныхСредств(ВидДокумента).Направление;
	Если НаправлениеДвижения = Неопределено тогда
		//Другие документы не обрабатываются
		Возврат Ложь;
	Конецесли;

	ВидОперации = ?(РеквизитыДокумента.Найти("ВидОперации")=Неопределено,Неопределено,Ссылка["ВидОперации"]);
	
	Если НЕ ЗначениеЗаполнено(Заголовок) тогда		
		СтрокаВидаОперации = ?(ВидОперации = Неопределено, "", ВидОперации);
		Заголовок = Ссылка.Метаданные().Синоним + ": " + СтрокаВидаОперации + ". ";
	КонецЕсли;
	
	РасчетыВозврат = ОпределениеНаправленияДвиженияДляДокументаДвиженияДенежныхСредств(ВидДокумента,ВидОперации).РасчетыВозврат;
	Если РасчетыВозврат = Неопределено тогда
		//Другие операции не обрабатываются
		Возврат Ложь;
	КонецЕсли;
	
	//Получение реквизитов шапки
	СтруктураПараметров = Новый Структура("Организация,ВалютаДокумента,ОтражатьВНалоговомУчете,ДатаОплаты");
	Для Каждого Реквизит из СтруктураПараметров Цикл
		СтруктураПараметров.Вставить(Реквизит.Ключ,?(РеквизитыДокумента.Найти(Реквизит.Ключ)=Неопределено,Неопределено,Ссылка[Реквизит.Ключ]));
	КонецЦикла;
	
	СтруктураПараметров.Вставить("НаправлениеДвижения", НаправлениеДвижения);
	СтруктураПараметров.Вставить("РасчетыВозврат"     , РасчетыВозврат);
	СтруктураПараметров.Вставить("ВидДокумента"       , ВидДокумента);
	СтруктураПараметров.Вставить("Регистратор"        , Ссылка);
	СтруктураПараметров.Вставить("Заголовок"          , Заголовок);

	Если НЕ ЗначениеЗаполнено(СтруктураПараметров["ДатаОплаты"]) Тогда
		СтруктураПараметров.Вставить("Период", Ссылка.Дата);
	Иначе
		СтруктураПараметров.Вставить("Период", СтруктураПараметров["ДатаОплаты"]);
	Конецесли;
	
	СтруктураКурсаДокумента = МодульВалютногоУчета.ПолучитьКурсВалюты(СтруктураПараметров["ВалютаДокумента"],СтруктураПараметров["Период"]);
	СтруктураПараметров.Вставить("КурсДокумента"     , СтруктураКурсаДокумента.Курс);
	СтруктураПараметров.Вставить("КратностьДокумента", СтруктураКурсаДокумента.Кратность);
	
	РасчетыВВалюте = Не СтруктураПараметров["ВалютаДокумента"]=ВалютаРегламентированногоУчета;
	
	// Получение данных таблицы расшифровки платежа
	// Выгрузка нужных колонок табличной части в таблицу значений с переименованием названий счетов
	СтруктураТаблицы = Новый Структура("СуммаПлатежа,ДоговорКонтрагента");

	
	СтруктураТаблицы.Вставить("ЗаТару", "ВозвратнаяТара");
	СтруктураТаблицы.Вставить("СуммаПлатежа", "СуммаСНДСВал");
	
	ТабличнаяЧасть = Ссылка["РасшифровкаПлатежа"];
	
	РеестрПлатежей = ОбщегоНазначения.СформироватьТаблицуЗначений(ТабличнаяЧасть, СтруктураТаблицы, Истина);
	Если НЕ ЗначениеЗаполнено(РеестрПлатежей) Тогда
		ОбщегоНазначения.СообщитьОбОшибке("Ошибка при движении по регистрам учета НДС. Отсутствуют необходимые реквизиты в расшифровке платежа.",Ложь,Заголовок);
		Возврат Ложь;
	КонецЕсли;
	
	РеестрПлатежей.Колонки.Добавить("СуммаСНДСРегл", ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(15,2));
	
	ДанныеВалютыРегламентированногоУчета = МодульВалютногоУчета.ПолучитьКурсВалюты(ВалютаРегламентированногоУчета, СтруктураПараметров.Период);

	Для Каждого СтрокаТаблицы Из РеестрПлатежей Цикл
		
		Если РасчетыВВалюте Тогда
			
			СтрокаТаблицы.СуммаСНДСРегл = МодульВалютногоУчета.ПересчитатьИзВалютыВВалюту(СтрокаТаблицы.СуммаСНДСВал, 
										         				СтруктураПараметров.ВалютаДокумента, ВалютаРегламентированногоУчета,
													            СтруктураПараметров.КурсДокумента, ДанныеВалютыРегламентированногоУчета.Курс, 
														        СтруктураПараметров.КратностьДокумента, ДанныеВалютыРегламентированногоУчета.Кратность);
																
		Иначе
			
			СтрокаТаблицы.СуммаСНДСРегл = СтрокаТаблицы.СуммаСНДСВал;
			
		КонецЕсли;
		
	КонецЦикла;

	СтруктураПараметров.Вставить("Таблица", РеестрПлатежей);
	
	Возврат СтруктураПараметров;

КонецФункции

Функция ОпределениеНаправленияДвиженияДляДокументаДвиженияДенежныхСредств(ВидДокумента,ВидОперации = Неопределено)

	ВидДействийДокумента = Новый Структура("Направление,РасчетыВозврат");
	ВидыДокументовДДС    = Новый Соответствие();

	ВидДвиженияПоступление = Перечисления.НаправленияДвижений.Поступление;
	ВидДвиженияВыбытие     = Перечисления.НаправленияДвижений.Выбытие;
	
	ВидыДокументовДДС.Вставить("АккредитивПереданный"                    , ВидДвиженияВыбытие);
	ВидыДокументовДДС.Вставить("ПлатежноеПоручениеИсходящее"             , ВидДвиженияВыбытие);
	ВидыДокументовДДС.Вставить("ПлатежноеТребованиеПолученное"           , ВидДвиженияВыбытие);
	ВидыДокументовДДС.Вставить("ПлатежноеТребованиеПоручениеПолученное"  , ВидДвиженияВыбытие);
	ВидыДокументовДДС.Вставить("ПлатежныйОрдерСписаниеДенежныхСредств"   , ВидДвиженияВыбытие);
	ВидыДокументовДДС.Вставить("РасходныйКассовыйОрдер"                  , ВидДвиженияВыбытие);
	
	ВидыДокументовДДС.Вставить("АккредитивПолученный"                    , ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ПлатежноеПоручениеВходящее"              , ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ПлатежноеТребованиеВыставленное"         , ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ПлатежноеТребованиеПоручениеВыставленное", ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ПлатежныйОрдерПоступлениеДенежныхСредств", ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ПриходныйКассовыйОрдер"                  , ВидДвиженияПоступление);
	ВидыДокументовДДС.Вставить("ОплатаОтПокупателяПлатежнойКартой"		 , ВидДвиженияПоступление);
	
	ВидДействийДокумента.Вставить("Направление",ВидыДокументовДДС[ВидДокумента]);

	ВидОперацииРасчет  = Перечисления.РасчетыВозврат.Расчеты;
	ВидОперацииВозврат = Перечисления.РасчетыВозврат.Возврат;
	
	Если ЗначениеЗаполнено(ВидОперации) Тогда

		//Определение вида операции

		РасчетВозвратПоВидуОпераций = Новый Соответствие();

		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийРКО.ОплатаПоставщику                                                   , ВидОперацииРасчет);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийРКО.ВозвратДенежныхСредствПокупателю                                   , ВидОперацииВозврат);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийРКО.ПрочиеРасчетыСКонтрагентами                                        , ВидОперацииРасчет);
		
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПКО.ОплатаПокупателя                                                   , ВидОперацииРасчет);
        РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПКО.ВозвратДенежныхСредствПоставщиком                                  , ВидОперацииВозврат);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПКО.ПрочиеРасчетыСКонтрагентами		                                 , ВидОперацииРасчет);
		
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийСписаниеБезналичныхДенежныхСредств.ОплатаПоставщику                    , ВидОперацииРасчет);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийСписаниеБезналичныхДенежныхСредств.ВозвратДенежныхСредствПокупателю    , ВидОперацииВозврат);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийСписаниеБезналичныхДенежныхСредств.ПрочиеРасчетыСКонтрагентами         , ВидОперацииРасчет);
		
		
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПоступлениеБезналичныхДенежныхСредств.ОплатаПокупателя                 , ВидОперацииРасчет);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПоступлениеБезналичныхДенежныхСредств.ВозвратДенежныхСредствПоставщиком, ВидОперацииВозврат);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийПоступлениеБезналичныхДенежныхСредств.ПрочиеРасчетыСКонтрагентами		 , ВидОперацииРасчет);
		
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийППИсходящее.ОплатаПоставщику                                           , ВидОперацииРасчет);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийППИсходящее.ВозвратДенежныхСредствПокупателю                           , ВидОперацииВозврат);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийППИсходящее.ПрочиеРасчетыСКонтрагентами								 ,ВидОперацииРасчет);
		
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийОплатаОтПокупателяПлатежнойКартой.ОплатаПокупателя,ВидОперацииРасчет);
		РасчетВозвратПоВидуОпераций.Вставить(Перечисления.ВидыОперацийОплатаОтПокупателяПлатежнойКартой.ВозвратДенежныхСредствПокупателю,ВидОперацииВозврат);
		
		Если ВидОперации = Перечисления.ВидыОперацийОплатаОтПокупателяПлатежнойКартой.ВозвратДенежныхСредствПокупателю Тогда
			ВидДействийДокумента.Вставить("Направление",ВидДвиженияВыбытие);
		КонецЕсли; 
		
		ВидДействийДокумента.Вставить("РасчетыВозврат", РасчетВозвратПоВидуОпераций[ВидОперации]);
		
	Конецесли;

	Возврат ВидДействийДокумента;

КонецФункции

// Функция возвращает ставку НДС.
//
// Параметры:
//  СтавкаНДС - Ставка НДС.
//
// Возвращаемое значение:
//  Число.
//
Функция ПолучитьСтавкуНДС(СтавкаНДС) Экспорт

	Если СтавкаНДС = Перечисления.СтавкиНДС.НДС20 Тогда
		Возврат 20;
	КонецЕсли;

	Возврат 0;

КонецФункции // ПолучитьСтавкуНДС()

// Рассчитывает сумму НДС исходя из суммы и флагов налогообложения
//
// Параметры: 
//  Сумма            - число, сумма от которой надо рассчитывать налоги, 
//  УчитыватьНДС     - булево, признак учета НДС в сумме, 
//  СуммаВключаетНДС - булево, признак включения НДС в сумму ("внутри" или "сверху"),
//  СтавкаНДС        - число , процентная ставка НДС,
//
// Возвращаемое значение:
//  Число, полученная сумма НДС
//
Функция РассчитатьСуммуНДС(Сумма, УчитыватьНДС, СуммаВключаетНДС, СтавкаНДС) Экспорт

	Если (УчитыватьНДС) И (СуммаВключаетНДС) Тогда
		СуммаБезНДС = 100 * Сумма / (100 + СтавкаНДС);
		СуммаНДС = Сумма - СуммаБезНДС;
	Иначе
		СуммаБезНДС = Сумма;
	КонецЕсли;

	Если УчитыватьНДС Тогда 
		Если НЕ СуммаВключаетНДС Тогда
			СуммаНДС = СуммаБезНДС * СтавкаНДС / 100;
		КонецЕсли;
	Иначе
		СуммаНДС = 0;
	КонецЕсли;

	Возврат СуммаНДС;

КонецФункции // РассчитатьСуммуНДС()

// Рассчитывает сумму НДС исходя из суммы и флагов налогообложения
//
// Параметры: 
//  Сумма            - число, сумма от которой надо рассчитывать налоги, 
//  УчитыватьНДС     - булево, признак учета НДС в сумме, 
//  СуммаВключаетНДС - булево, признак включения НДС в сумму ("внутри" или "сверху"),
//  СтавкаНДС        - число , процентная ставка НДС,
//
// Возвращаемое значение:
//  Число, полученная сумма НДС
//
Функция РассчитатьСуммуНДСсУчетомПогрешности(Сумма, УчитыватьНДС, СуммаВключаетНДС, ПеречислениеСтавкаНДС,
	               СоответствиеПогрешностей = Неопределено) Экспорт

	СтавкаНДС = ПолучитьСтавкуНДС(ПеречислениеСтавкаНДС);
				   
	Если УчитыватьНДС Тогда 
		Если СуммаВключаетНДС Тогда
			СуммаНДС = ОбщегоНазначения.ОкруглитьСУчетомПогрешности(Сумма * СтавкаНДС / (100 + СтавкаНДС), 2, , СоответствиеПогрешностей, ПеречислениеСтавкаНДС);
		Иначе
			СуммаНДС = ОбщегоНазначения.ОкруглитьСУчетомПогрешности(Сумма * СтавкаНДС / 100              , 2, , СоответствиеПогрешностей, ПеречислениеСтавкаНДС);
		КонецЕсли;
	Иначе
		СуммаНДС = 0;
	КонецЕсли;

	Возврат СуммаНДС;

КонецФункции // РассчитатьСуммуНДССУчетомПогрешности()

//Удалает повторяющиеся элементы массива.
Функция УдалитьПовторяющиесяЭлементыМассива(Массив, НеИспользоватьНеопределено = Ложь) Экспорт
	
	ОписаниеТиповСправочники  = Справочники.ТипВсеСсылки(); 
	ОписаниеТиповДокументы    = Документы.ТипВсеСсылки(); 
	ОписаниеТиповПВХ          = ПланыВидовХарактеристик.ТипВсеСсылки(); 

	Если ТипЗнч(Массив) = Тип("Массив") Тогда 
		
		УжеВМасссиве = Новый Соответствие; 
		БылоНеопределено = Ложь;
		
		КолвоЭлементовВМассиве = Массив.Количество(); 
		
		Для ОбратныйИндекс = 1 По КолвоЭлементовВМассиве Цикл 
			ЭлементМассива = Массив[КолвоЭлементовВМассиве - ОбратныйИндекс]; 
			ТипЭлемента = ТипЗнч(ЭлементМассива); 
			Если ЭлементМассива = Неопределено Тогда
				Если БылоНеопределено или НеИспользоватьНеопределено Тогда
					Массив.Удалить(КолвоЭлементовВМассиве - ОбратныйИндекс); 
				Иначе
					БылоНеопределено = Истина;
				КонецЕсли;
				Продолжить;
			ИначеЕсли ОписаниеТиповСправочники.СодержитТип(ТипЭлемента) 
				ИЛИ ОписаниеТиповДокументы.СодержитТип(ТипЭлемента) 
				ИЛИ ОписаниеТиповПВХ.СодержитТип(ТипЭлемента) Тогда
				
				ИДЭлемента = Строка(ЭлементМассива.УникальныйИдентификатор()); 
				
			Иначе 
				
				ИДЭлемента = ЭлементМассива; 
				
			КонецЕсли; 
			
			Если УжеВМасссиве[ИДЭлемента] = Истина Тогда 
				Массив.Удалить(КолвоЭлементовВМассиве - ОбратныйИндекс); 
			Иначе 
				УжеВМасссиве[ИДЭлемента] = Истина; 
			КонецЕсли; 
		КонецЦикла;      
		
	КонецЕсли;
	
	Возврат Массив;
	
КонецФункции

// Рассчитываем сумму документа со всеми налогами
//
// Параметры: 
//  ДокументОбъект    - объект документа, сумму которого надо рассчитать
//  ИмяТабличнойЧасти - строка, имя табличной части, сумму которой надо рассчитать.
//                      Если она не заполнена, считаем по всем табличным частям, в которых есть "Сумма"
//  НеУчитыватьТару   - булево, если Истина и ИмяТабличнойЧасти неопределено, то в расчете сумм тару не учитываем
//
// Возвращаемое значение:
//  Сумма документа со всеми налогами.
//
Функция ПолучитьСуммуДокументаСНДС(ДокументОбъект, ИмяТабличнойЧасти = Неопределено, НеУчитыватьТару = Истина) Экспорт

	МетаданныеДокумента = ДокументОбъект.Метаданные();

	СуммаДокумента = 0;
	Если ИмяТабличнойЧасти <> Неопределено Тогда
		СуммаДокумента = СуммаДокумента + ДокументОбъект[ИмяТабличнойЧасти].Итог("Сумма");
		Если ОбщегоНазначения.ЕстьРеквизитДокумента("УчитыватьНДС", МетаданныеДокумента)
		   И ОбщегоНазначения.ЕстьРеквизитДокумента("СуммаВключаетНДС", МетаданныеДокумента)
		   И ОбщегоНазначения.ЕстьРеквизитТабЧастиДокумента("СуммаНДС", МетаданныеДокумента, ИмяТабличнойЧасти)
		   И ДокументОбъект.УчитыватьНДС
		   И Не ДокументОбъект.СуммаВключаетНДС Тогда
			СуммаДокумента = СуммаДокумента + ДокументОбъект[ИмяТабличнойЧасти].Итог("СуммаНДС");
		КонецЕсли; 
	Иначе
		Для каждого ТЧОбъекта Из ДокументОбъект.Метаданные().ТабличныеЧасти Цикл
			Если НеУчитыватьТару и ТЧОбъекта.Имя = "ВозвратнаяТара" Тогда
				Продолжить;
			КонецЕсли;
			Если ОбщегоНазначения.ЕстьРеквизитТабЧастиДокумента("Сумма", МетаданныеДокумента, ТЧОбъекта.Имя) Тогда
				СуммаДокумента = СуммаДокумента + ДокументОбъект[ТЧОбъекта.Имя].Итог("Сумма");
				Если ОбщегоНазначения.ЕстьРеквизитДокумента("УчитыватьНДС", МетаданныеДокумента)
					И ОбщегоНазначения.ЕстьРеквизитДокумента("СуммаВключаетНДС", МетаданныеДокумента)
					И ОбщегоНазначения.ЕстьРеквизитТабЧастиДокумента("СуммаНДС", МетаданныеДокумента, ТЧОбъекта.Имя)
					И ДокументОбъект.УчитыватьНДС
					И Не ДокументОбъект.СуммаВключаетНДС Тогда
					СуммаДокумента = СуммаДокумента + ДокументОбъект[ТЧОбъекта.Имя].Итог("СуммаНДС");
				КонецЕсли; 
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

	Возврат СуммаДокумента;

КонецФункции // ПолучитьСуммуДокументаСНДС()

// Рассчитываем сумму НДС документа
//
// Параметры: 
//  ДокументОбъект    - объект документа, сумму которого надо рассчитать
//  ИмяТабличнойЧасти - строка, имя табличной части, сумму которой надо рассчитать.
//                      Если она не заполнена, считаем по всем табличным частям, в которых есть "Сумма"
//
// Возвращаемое значение:
//  НДС документа
//
Функция ПолучитьНДСДокумента(ДокументОбъект, ИмяТабличнойЧасти = Неопределено) Экспорт

	МетаданныеДокумента = ДокументОбъект.Метаданные();

	СуммаНДС = 0;
	Если ИмяТабличнойЧасти <> Неопределено Тогда
		СуммаНДС = СуммаНДС + ДокументОбъект[ИмяТабличнойЧасти].Итог("СуммаНДС");
	Иначе
		Для каждого ТЧОбъекта Из ДокументОбъект.Метаданные().ТабличныеЧасти Цикл
			Если ОбщегоНазначения.ЕстьРеквизитТабЧастиДокумента("СуммаНДС", МетаданныеДокумента, ТЧОбъекта.Имя) Тогда
				СуммаНДС = СуммаНДС + ДокументОбъект[ТЧОбъекта.Имя].Итог("СуммаНДС");
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

	Возврат СуммаНДС;

КонецФункции // ПолучитьНДСДокумента()