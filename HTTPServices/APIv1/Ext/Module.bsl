﻿Функция ШаблонURLPOST(HTTP_Запрос)
	
	Попытка
	
		СтрокаХМЛ = HTTP_Запрос.ПолучитьТелоКакСтроку();
		
		ХМЛ	= Новый ЧтениеXML;
		ХМЛ.УстановитьСтроку(СтрокаХМЛ);
		
		Дерево = Новый Структура();
		
		ОбщегоНазначения.ПостроитьДеревоXML(ХМЛ, Дерево);
		
		ИмяМетода = HTTP_Запрос.ОтносительныйURL;
		
		СохранитьСервисНаДиск(ИмяМетода, СтрокаХМЛ);
			
		СтруктураЗапроса = Дерево.request;	         
		Ключ = СтруктураЗапроса.auth;
		
		Если Ключ = "pEbSS6Y1gQiVswCtSWCe" Тогда			
			ОтветСтруктура = ОбщиеСерверHTTP.ВыборМетода(ИмяМетода, СтруктураЗапроса);			
		Иначе
			
			//ОтветСтруктура = Новый Структура;
			//	
			//ОтветСтруктура.Вставить("XML_Ответа","Неверный ключ API");
			//ОтветСтруктура.Вставить("Заголовки","application/xml");
			//ОтветСтруктура.Вставить("КодСостояния",401);
			
			НашОтвет = "Неверный ключ API";
			ОтветСтруктура = ОбщиеСерверHTTP.Вернуть500(НашОтвет);
			ОтветСтруктура.Вставить("КодСостояния", 401);
		  
		КонецЕсли;
		
	Исключение	
		
		//ОтветСтруктура = Новый Структура;
		//    	
		//ОтветСтруктура.Вставить("XML_Ответа", "Общая ошибка API");
		//ОтветСтруктура.Вставить("Заголовки", "application/xml");
		//ОтветСтруктура.Вставить("КодСостояния", 500);
		
		НашОтвет = "Общая ошибка API";
		ОтветСтруктура = ОбщиеСерверHTTP.Вернуть500(НашОтвет);
		
	КонецПопытки;	
							
	Если ОтветСтруктура.Свойство("МассивОшибок") Тогда
				
		Если ОтветСтруктура.Свойство("КодСостояния") Тогда
			Ответ = Новый HTTPСервисОтвет(ОтветСтруктура.КодСостояния);
		Иначе
	    	Ответ = Новый HTTPСервисОтвет(500);
		КонецЕсли;
		
		Ответ.Заголовки.Вставить("Content-type", "application/xml");
	    			
		СводныйТекстОшибки = "";
			
		Для каждого ТекстРока из ОтветСтруктура.МассивОшибок Цикл
			СводныйТекстОшибки = СводныйТекстОшибки + СокрЛП(ТекстРока) + ", ";
		КонецЦикла;
			
		СводныйТекстОшибки = СокрЛП(СводныйТекстОшибки);				
		ДлинаСообщенияОбОшибке = СтрДлина(СводныйТекстОшибки);				
		СводныйТекстОшибки = Лев(СводныйТекстОшибки, ДлинаСообщенияОбОшибке - 1);				
		Ответ.УстановитьТелоИзСтроки("<response>" + СводныйТекстОшибки + "</response>");
		
	ИначеЕсли ОтветСтруктура.Свойство("XML_Ответа") Тогда
					
		Если ОтветСтруктура.Заголовки = "application/xml" Тогда 
																			
			Если ОтветСтруктура.Свойство("КодСостояния") Тогда
				Ответ = Новый HTTPСервисОтвет(ОтветСтруктура.КодСостояния);
			Иначе
	    		Ответ = Новый HTTPСервисОтвет(200);
			КонецЕсли;
			
			Ответ.Заголовки.Вставить("Content-type", ОтветСтруктура.Заголовки);
			Ответ.УстановитьТелоИзСтроки(ОтветСтруктура.XML_Ответа);
			
		ИначеЕсли ОтветСтруктура.Заголовки = "application/vnd.ms-excel" Тогда 
			
			Ответ = Новый HTTPСервисОтвет(200);
			
			Ответ.Заголовки.Вставить("Content-type", ОтветСтруктура.Заголовки);
			Ответ.УстановитьИмяФайлаТела(ОтветСтруктура.XML_Ответа);
	    					
		КонецЕсли;
		
	Иначе	
		
		Ответ = Новый HTTPСервисОтвет(200);
		
		Ответ.Заголовки.Вставить("Content-type", "application/xml");
		Ответ.УстановитьТелоИзСтроки("<response>ok</response>");
	    			
	КонецЕсли;	
							
	Возврат Ответ;

КонецФункции

Функция ШаблонURLGET(Запрос)				
					
	НашОтвет = "Hallo, Welt";
						
	Ответ = Новый HTTPСервисОтвет(200);
		
	Ответ.УстановитьТелоИзСтроки(НашОтвет);
				
	Возврат Ответ;

КонецФункции

Процедура СохранитьСервисНаДиск(ИмяМетода, ТекстСервиса)
	
	//Если Лев(ИмяМетода,2) = "\\" Тогда 
	//	ИмяСервиса = "\Unbeknnt_1";
	//ИначеЕсли Лев(ИмяМетода,1) = "/" Тогда 
	//	ИмяСервиса = ИмяМетода;
	//Иначе 
	//	ИмяСервиса = "\Unbeknnt_2";
	//КонецЕсли;	
	//
	//ТекущееВремя = ТекущаяДата();
	//ЛитералДаты = СтрЗаменить(Формат(ТекущаяДата(),"ДЛФ=DT"),":",".");
	//
	//ИмяФайла = "C:\Temp\1c" + ИмяСервиса + "_" + ЛитералДаты + ".xml";
	//
	//ФайлНаДиске = Новый ЗаписьТекста(ИмяФайла);
	//ФайлНаДиске.ЗаписатьСтроку(ТекстСервиса);
	//ФайлНаДиске.Закрыть();
	
	ВремяСозданияФайла = ТекущаяДата();
	
	ЛитералКраткий = Формат(ВремяСозданияФайла,"Л=ru_UA; ДЛФ=DT");
	ЛитералПолный = Формат(ВремяСозданияФайла,"Л=ru_UA; ДЛФ=DD");
	
	Если Лев(ИмяМетода,2) = "\\" Тогда 
		ИмяСервиса = "\Unbeknnt_1";
	ИначеЕсли Лев(ИмяМетода,1) = "/" Тогда 
		ИмяСервиса = ИмяМетода;
	Иначе 
		ИмяСервиса = "\Unbeknnt_2";
	КонецЕсли;
	
	ПолныйПуть = "C:\Temp\1c" + ИмяСервиса + "\" + ЛитералПолный;
	
	ПоискПапки = новый Файл(ПолныйПуть);
	
	Если НЕ ПоискПапки.Существует() Тогда 		
		СоздатьКаталог(ПолныйПуть);		
	КонецЕсли;	
    		
	ЛитералДаты = СтрЗаменить(ЛитералКраткий,":",".");
	
	ИмяФайла = ПолныйПуть + ИмяСервиса + "_" + ЛитералДаты + ".xml";
	
	ФайлНаДиске = Новый ЗаписьТекста(ИмяФайла);
	ФайлНаДиске.ЗаписатьСтроку(ТекстСервиса);
	ФайлНаДиске.Закрыть();
	
КонецПроцедуры	
